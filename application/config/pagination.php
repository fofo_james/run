<?php

$config['enable_query_strings'] = TRUE;
$config['use_page_numbers'] = TRUE;
	
$config['page_query_string'] = TRUE; //將分頁連結改為?c=test&m=page&per_page=20
$config['query_string_segment'] = 'page'; //改變預設分頁字串per_page

$config['full_tag_open'] = '<ul class="pagination">';
$config['full_tag_close'] = '</ul>';
 
$config['first_link'] = '&laquo;';//自訂開始分頁連結名稱
$config['first_tag_open'] = '<li class="prev page">';
$config['first_tag_close'] = '</li>';
 
$config['last_link'] = '&raquo;'; //自訂結束分頁連結名稱
$config['last_tag_open'] = '<li class="next page">';
$config['last_tag_close'] = '</li>';
 
$config['next_link'] = '下一頁 >';
$config['next_tag_open'] = '<li class="next page">'; //自訂下一頁標籤
$config['next_tag_close'] = '</li>';
 
$config['prev_link'] = '< 上一頁';
$config['prev_tag_open'] = '<li class="prev page">';
$config['prev_tag_close'] = '</li>';
$config['cur_tag_open'] = '<li class="active"><a>';
$config['cur_tag_close'] = '</a></li>';
$config['num_tag_open'] = '<li class="page">';
$config['num_tag_close'] = '</li>';
			// $config['query_string_segment'] = 'your_string';
?>