<?php 
class Api extends CI_Controller{
	function __construct() {
		parent::__construct();
		$this->load->model('mod_member');
		$this->load->helper('email');
		// echo '123';
	}

	function db_to_json(){
		$this->db->group_by('city');
		$this->db->select('city');
		$res = array();
		foreach ($this->db->get('exp')->result_array() as $key => $value) {
			# code...
			$res[$key]['city'] = $value['city'];
			$res[$key]['desc'] = $this->db->where('city',$value['city'])->get('exp')->result_array();
		}
		$json_arr = $res;
		echo json_encode($json_arr);
	}
/**
* ==========================================================================================
*	會員功能
**/
// reg_member 註冊一般會員
	function reg_member(){
		$email = $this->input->get_post('email');
		if (!valid_email($email))
		{
		   $email = '';
		}
		$password = $this->input->get_post('password');
		$username = $this->input->get_post('username');
		$birthday = $this->input->get_post('birthday');
		$gender = $this->input->get_post('gender');
		$height = $this->input->get_post('height');
		$weight = $this->input->get_post('weight');
		$avator = $this->input->get_post('avator');

		if($email == '' || $password == ''){
			$json_arr['sys_code'] = '000';
			$json_arr['sys_msg'] = '參數不足或格式錯誤';
		}elseif($this->mod_member->chk_once($email)){
			$json_arr['sys_code'] = '500';
			$json_arr['sys_msg'] = 'Please use Login';
		}else{
			$member_id = "M".time();
			$data = array(
				"member_id"=>$member_id,
				"email"=>$email,
				"password"=>$password,
				"username"=>$username,
				"birthday"=>$birthday,
				"gender"=>$gender,
				"height"=>$height,
				"weight"=>$weight,
				"avator"=>$avator,
				);
			$this->mod_member->add_once($data);
			$json_arr['sys_code'] = '200';
			$json_arr['sys_msg'] = '註冊完成';
			$json_arr['member_id'] = $member_id;
		}
		echo json_encode($json_arr);
	}



// fb_reg_member FB會員  會員註冊
	function fb_reg_member(){
		$email = $this->input->get_post('email');
		if (!valid_email($email))
		{
		   $email = '';
		}
		$fbid = $this->input->get_post('fbid');

		if($email == '' || $fbid == ''){
			$json_arr['sys_code'] = '000';
			$json_arr['sys_msg'] = '參數不足或格式錯誤';
		}elseif ($this->mod_member->chk_once_by_fbid($fbid)) {
			# code...
			$json_arr['sys_code'] = '500';
			$json_arr['sys_msg'] = ' You`re already member, please use login.';
		}elseif($this->mod_member->chk_once($email)){
			$json_arr['sys_code'] = '500';
			$json_arr['sys_msg'] = 'email 已經被註冊過';
		}else{
			$member_id = $this->mod_member->fb_reg($fbid,$email);

			$username = $this->input->get_post('username');
			$birthday = $this->input->get_post('birthday');
			$gender = $this->input->get_post('gender');
			$height = $this->input->get_post('height');
			$weight = $this->input->get_post('weight');
			$avator = $this->input->get_post('avator');

			if($username != ''){
				$data['username']=$username;
			}
			if($birthday != ''){
				$data['birthday']=$birthday;
			}
			if($gender != ''){
				$data['gender']=$gender;
			}
			if($height != ''){
				$data['height']=$height;
			}
			if($weight != ''){
				$data['weight']=$weight;
			}
			if($avator != ''){
				$data['avator']=$avator;
			}
			$this->mod_member->update_once($member_id,$data);
			$json_arr['last_query'] = $this->db->last_query();

			$json_arr['sys_code'] = '200';
			$json_arr['sys_msg'] = '註冊完成';
			$json_arr['member_id'] = $member_id;
		}
		echo json_encode($json_arr);
	}



// upload_avator 上傳個人頭像
	function upload_avator(){
		if($_FILES['avator']['size'] == 0){
			$json_arr['sys_code'] = '000';
			$json_arr['sys_msg'] = '參數不足或格式錯誤';
		}else{
			$file = "avator/".time().'.png';
			copy($_FILES['avator']['tmp_name'],$file);
			if(file_exists($file)){
				$json_arr['sys_code'] = '200';
				$json_arr['sys_msg'] = '上傳完成';
				$json_arr['avator'] = base_url($file);
			}else{
				$json_arr['sys_code'] = '500';
				$json_arr['sys_msg'] = '意外錯誤';
			}
		}
		echo json_encode($json_arr);
	}

	function upload_member_background(){
		$member_id = $this->input->get_post('member_id');
		// print_r($_FILES);
		if($_FILES['background']['size'] == 0){
			$json_arr['sys_code'] = '000';
			$json_arr['sys_msg'] = '參數不足或格式錯誤';
		}elseif(!$this->mod_member->chk_once_by_id($member_id)){
			$json_arr['sys_code'] = '404';
			$json_arr['sys_msg'] = '查無此人';
		}else{
			$file = "background/".uniqid().'.png';
			copy($_FILES['background']['tmp_name'],$file);
			if(file_exists($file)){
				$json_arr['sys_code'] = '200';
				$json_arr['sys_msg'] = '上傳完成';
				$json_arr['background'] = base_url($file);
				$this->mod_member->update_once($member_id,array('background'=>$json_arr['background']));
			}else{
				$json_arr['sys_code'] = '500';
				$json_arr['sys_msg'] = '意外錯誤';
			}
		}
		echo json_encode($json_arr);
	}


// login 會員登入 FB 和一般共用
	function login(){
		$email = $this->input->get_post('email');
		if (!valid_email($email))
		{
		   $email = '';
		}
		$password = $this->input->get_post('password');
		$fbid = $this->input->get_post('fbid');
		$type = $this->input->get_post('type');

		if(!$this->mod_member->chk_once_by_fbid($fbid)){
			$json_arr['sys_code'] = '404';
			$json_arr['sys_msg'] = '查無此人';
		}else{
			switch ($type) {
				case 'general':
					# code...
					if($password == ""){
						$json_arr['sys_code'] = '000';
						$json_arr['sys_msg'] = '參數不足或格式錯誤';
					}else{
						$member_id = $this->mod_member->chk_login_general($email,$password);
						if($member_id != false){
							$json_arr['sys_code'] = '200';
							$json_arr['sys_msg'] = '登入完成';
							$json_arr['member_id'] = $member_id;
						}else{
							$json_arr['sys_code'] = '500';
							$json_arr['sys_msg'] = '驗證錯誤 登入失敗';
						}
					}
					break;
				case 'fb':
					# code...
					if($fbid == ""){
						$json_arr['sys_code'] = '000';
						$json_arr['sys_msg'] = '參數不足或格式錯誤';
					}else{
						$member_id = $this->mod_member->chk_login_fb($email,$fbid);
						if($member_id != false){
							$json_arr['sys_code'] = '200';
							$json_arr['sys_msg'] = '登入完成';
							$json_arr['member_id'] = $member_id;
						}else{
							$json_arr['sys_code'] = '500';
							$json_arr['sys_msg'] = '驗證錯誤 登入失敗';
						}
					}
					
				break;
				
				default:
					$json_arr['sys_code'] = '501';
					$json_arr['sys_msg'] = '登入方式錯誤';
					break;
			}
		}
		echo json_encode($json_arr);
	}


	// passwd 設定密碼
	function passwd(){
		$member_id = $this->input->get_post('member_id');
		$password = $this->input->get_post('password');
		if($member_id == "" || $password == ""){
			$json_arr['sys_code'] = '000';
			$json_arr['sys_msg'] = '參數不足或格式錯誤';
		}elseif(!$this->mod_member->chk_once_by_id($member_id)){
			$json_arr['sys_code'] = '404';
			$json_arr['sys_msg'] = '查無此人';
		}else{
			$this->mod_member->update_once($member_id,array('password'=>$password));
			$json_arr['sys_code'] = '200';
			$json_arr['sys_msg'] = '完成';
		}
		echo json_encode($json_arr);
	}

	// get_member 取得個人資料
	function get_member(){
		$member_id = $this->input->get_post('member_id');
		if($member_id == ''){
			$json_arr['sys_code'] = '000';
			$json_arr['sys_msg'] = '參數不足或格式錯誤';
		}elseif(!$this->mod_member->chk_once_by_id($member_id)){
			$json_arr['sys_code'] = '404';
			$json_arr['sys_msg'] = '查無此人';
		}else{
			$json_arr['sys_code'] = '200';
			$json_arr['sys_msg'] = '完成';
			$json_arr['member_info'] = $this->mod_member->get_once(array('member_id'=>$member_id));
		}
		echo json_encode($json_arr);
	}

	// 設定個人分數
	function set_member_score(){
		$member_id = $this->input->get_post('member_id');
		$score = $this->input->get_post('score');
		$class = $this->input->get_post('class');
		if($member_id == '' || $score =="" || $class == ""){
			$json_arr['sys_code'] = '000';
			$json_arr['sys_msg'] = '參數不足或格式錯誤';
		}elseif(!$this->mod_member->chk_once_by_id($member_id)){
			$json_arr['sys_code'] = '404';
			$json_arr['sys_msg'] = '查無此人';
		}else{
			$this->mod_member->update_score($member_id,$score,$class);
			$json_arr['sys_code'] = '200';
			$json_arr['sys_msg'] = '完成';
			
		}
		echo json_encode($json_arr);
	}
	// 查詢好友資料
	function get_friend_info(){
		$this->load->model('mod_friend');
		$my = $this->input->get_post('my');
		$target = $this->input->get_post('target');
		if($my == '' || $target == ""){
			$json_arr['sys_code'] = '000';
			$json_arr['sys_msg'] = '參數不足或格式錯誤';
		}elseif(!$this->mod_member->chk_once_by_id($my)){
			$json_arr['sys_code'] = '404';
			$json_arr['sys_msg'] = '查無自己資料';
		}elseif(!$this->mod_member->chk_once_by_id($target)){
			$json_arr['sys_code'] = '404';
			$json_arr['sys_msg'] = '查無對方資料';
		}else{
			$json_arr['sys_code'] = '200';
			$json_arr['sys_msg'] = '完成';
			$json_arr['member_info'] = $this->mod_member->get_once(array('member_id'=>$target));
			$json_arr['friend_status'] = $this->mod_friend->chk_friend($my,$target);
		}
		echo json_encode($json_arr);
	}

	// edit_member 編輯個人資料
	function edit_member(){
		$member_id = $this->input->get_post('member_id');
		
		$username = $this->input->get_post('username');
		$birthday = $this->input->get_post('birthday');
		$gender = $this->input->get_post('gender');
		$height = $this->input->get_post('height');
		$weight = $this->input->get_post('weight');
		$avator = $this->input->get_post('avator');

		if($member_id == ''){
			$json_arr['sys_code'] = '000';
			$json_arr['sys_msg'] = '參數不足或格式錯誤';
		}elseif(!$this->mod_member->chk_once_by_id($member_id)){
			$json_arr['sys_code'] = '404';
			$json_arr['sys_msg'] = '查無此人';
		}else{
			if($username != ''){
				$data['username']=$username;
			}
			if($birthday != ''){
				$data['birthday']=$birthday;
			}
			if($gender != ''){
				$data['gender']=$gender;
			}
			if($height != ''){
				$data['height']=$height;
			}
			if($weight != ''){
				$data['weight']=$weight;
			}
			if($avator != ''){
				$data['avator']=$avator;
			}
			$this->mod_member->update_once($member_id,$data);

			$json_arr['sys_code'] = '200';
			$json_arr['sys_msg'] = '完成';
			$json_arr['member_info'] = $this->mod_member->get_once(array('member_id'=>$member_id));
		}
		echo json_encode($json_arr);
	}


		/** =================================*
		*									*
		* 感應器綁定 							*
		===================================**/ 
		// 感應器綁定
		function mapping_sensor(){
			$member_id = $this->input->get_post('member_id');
			$sensor_mac = $this->input->get_post('sensor_mac');
			$device_name = $this->input->get_post('device_name');
			$this->load->model('mod_sensor');

			if($member_id == "" || $sensor_mac == "" || $device_name == ""){
				// $json_arr['sys_code'] = '000';
				// $json_arr['sys_msg'] = '參數不足或格式錯誤';
				$json_arr['sys_code'] = '200';
				$json_arr['sys_msg'] = '處理完成';
			}elseif(!$this->mod_member->chk_once_by_id($member_id)){
				// $json_arr['sys_code'] = '404';
				// $json_arr['sys_msg'] = '查無此人';
				$json_arr['sys_code'] = '200';
				$json_arr['sys_msg'] = '處理完成';
			}elseif($this->mod_sensor->chk_sensor($sensor_mac)){
				// $json_arr['sys_code'] = '501';
				// $json_arr['sys_msg'] = '裝置重複綁定';
				$json_arr['sys_code'] = '200';
				$json_arr['sys_msg'] = '處理完成';
			}else{
				$this->mod_sensor->mapping_sensor($sensor_mac,$member_id,$device_name);
				$json_arr['sys_code'] = '200';
				$json_arr['sys_msg'] = '處理完成';
			}
			echo json_encode($json_arr);
		}

		function list_sensor(){
			$member_id = $this->input->get_post('member_id');
			$this->load->model('mod_sensor');
			if($member_id == "" ){
				$json_arr['sys_code'] = '000';
				$json_arr['sys_msg'] = '參數不足或格式錯誤';
			}elseif(!$this->mod_member->chk_once_by_id($member_id)){
				$json_arr['sys_code'] = '404';
				$json_arr['sys_msg'] = '查無此人';
			}else{
				$json_arr['sensors'] =$this->mod_sensor->get_list($member_id);
				$json_arr['sys_code'] = '200';
				$json_arr['sys_msg'] = '處理完成';
			}
			echo json_encode($json_arr);
		}

		function unlink_sensor(){
			$member_id = $this->input->get_post('member_id');
			$sensor_mac = $this->input->get_post('sensor_mac');
			$this->load->model('mod_sensor');

			if($member_id == "" || $sensor_mac == "" ){
				$json_arr['sys_code'] = '000';
				$json_arr['sys_msg'] = '參數不足或格式錯誤';
			}elseif(!$this->mod_member->chk_once_by_id($member_id)){
				$json_arr['sys_code'] = '404';
				$json_arr['sys_msg'] = '查無此人';
			}elseif(!$this->mod_sensor->chk_sensor($sensor_mac)){
				$json_arr['sys_code'] = '404';
				$json_arr['sys_msg'] = '裝置不存在';
			}else{
				$this->mod_sensor->unlink_sensor($sensor_mac,$member_id);
				$json_arr['sys_code'] = '200';
				$json_arr['sys_msg'] = '處理完成';
			}
			echo json_encode($json_arr);

		}


		/** =================================*
		*									*
		* HISTORY 記錄 						*
		===================================**/ 
		function history_save(){
			$member_id = $this->input->get_post('member_id');
			$sensor_mac = $this->input->get_post('sensor_mac');
			$date_time = $this->input->get_post('date_time');
			$milles = $this->input->get_post('milles');
			$cadence = $this->input->get_post('cadence');
			$min_mi = $this->input->get_post('min_mi');
			$note = $this->input->get_post('note');
			// $picture = $this->input->get_post('picture');
			$detail_list = $this->input->get_post('detail_list');
			$times = $this->input->get_post("times");

			$this->load->model('mod_history');
			$this->load->model('mod_sensor');

			if($member_id == "" || $sensor_mac == "" ){
				$json_arr['sys_code'] = '000';
				$json_arr['sys_msg'] = '參數不足或格式錯誤';
			}elseif(!$this->mod_member->chk_once_by_id($member_id)){
				$json_arr['sys_code'] = '404';
				$json_arr['sys_msg'] = '查無此人';
			}elseif(!$this->mod_sensor->chk_sensor($sensor_mac)){
				$json_arr['sys_code'] = '404';
				$json_arr['sys_msg'] = '裝置不存在';
			}else{
				$file = "history/".uniqid().'.png';
				if(empty($_FILES['picture'])){
					$picture = '';
				}else{
					copy($_FILES['picture']['tmp_name'],$file);
					if(file_exists($file)){
						$picture = base_url($file);
					}else{
						$picture = '';
					}
				}
				

				$data = array(
					"member_id"=>$member_id,
					"sensor_mac"=>$sensor_mac,
					"date_time"=>$date_time,
					"milles"=>$milles,
					"times"=>$times,
					"cadence"=>$cadence,
					"min_mi"=>$min_mi,
					"note"=>$note,
					"picture"=>$picture,
					"detail_list"=>$detail_list,
					);
				$hid = $this->mod_history->add_once($data);
				if(!is_dir('./csv/'.$member_id.'/')){
					mkdir('./csv/'.$member_id.'/','0777');

				}
				exec('chmod -R 777 ./csv/'.$member_id.'/');
				if(isset($_FILES['foreProfile']['tmp_name'])){
					copy($_FILES['foreProfile']['tmp_name'], './csv/'.$member_id.'/foreProfile_'.$hid.'.csv');
				}
				if(isset($_FILES['heelProfile']['tmp_name'])){
					copy($_FILES['heelProfile']['tmp_name'], './csv/'.$member_id.'/heelProfile_'.$hid.'.csv');
				}

				//新增觸發 server 運算
				$curl = 'curl http://61.220.51.232:5000/users -d "uid='.$member_id.'&url=http://61.220.51.232/csv/'.$member_id.'/Profile_'.$hid.' -X POST -v';
				exec($curl);

				$json_arr['sys_code'] = '200';
				$json_arr['sys_msg'] = '處理完成';

				
			}
			echo json_encode($json_arr);



		}

		function history_list(){
			$member_id = $this->input->get_post('member_id');
			$sensor_mac = $this->input->get_post('sensor_mac');
			$qty = $this->input->get_post('qty');
			$this->load->model('mod_history');
			if($member_id == "" ){
				$json_arr['sys_code'] = '000';
				$json_arr['sys_msg'] = '參數不足或格式錯誤';
			}elseif(!$this->mod_member->chk_once_by_id($member_id)){
				$json_arr['sys_code'] = '404';
				$json_arr['sys_msg'] = '查無此人';
			}else{
				$history_list = $this->mod_history->get_list($member_id,$sensor_mac,$qty);
				if($history_list == false){
					$json_arr['history_list'] = array();
				}else{
					$json_arr['history_list']= $history_list;
				}
				$json_arr['sys_code'] = '200';
				$json_arr['sys_msg'] = '處理完成';
			}
			echo json_encode($json_arr);
		}

		function history_detail(){
			$hid = $this->input->get_post('hid');
			$this->load->model('mod_history');
			if($hid == ""){
				$json_arr['sys_code'] = '000';
				$json_arr['sys_msg'] = '參數不足或格式錯誤';
			}else{
				$json_arr['main'] = $this->mod_history->get_once($hid);
				$json_arr['main']['foreProfile'] = base_url('./csv/'.$json_arr['main']['hid'].'_f.csv');
				$json_arr['main']['heelProfile'] = base_url('./csv/'.$json_arr['main']['hid'].'_h.csv');
				$json_arr['sys_code'] = '200';
				$json_arr['sys_msg'] = '處理完成';
			}
			echo json_encode($json_arr);
		}
		

/** -------  -------------------
*
* 動態牆
----------------------------****/
	function newsfeed_add(){
		$this->load->model('mod_newsfeed');
		$newsfeed_id = uniqid();
		$member_id = $this->input->get_post('member_id');
		$times = $this->input->get_post('times');
		$milles = $this->input->get_post('milles');
		$note = $this->input->get_post('note');
		if($member_id == '' ){
			$json_arr['sys_code'] = '000';
			$json_arr['sys_msg'] = '參數不足或格式錯誤';
		}elseif(!$this->mod_member->chk_once_by_id($member_id)){
				$json_arr['sys_code'] = '404';
				$json_arr['sys_msg'] = '查無此人';
		}else{
			$file = "newsfeed/".$newsfeed_id.'.png';
			copy($_FILES['picture']['tmp_name'],$file);
			if(file_exists($file)){
				$picture = base_url($file);
				
			}else{
				$picture = '';
			}


			$data = array(
				"newsfeed_id"=>$newsfeed_id,
				"member_id"=>$member_id,
				"times"=>$times,
				"milles"=>$milles,
				"note"=>$note,
				"picture"=>$picture,
				"date_time"=>date("Y-m-d H:i:s")
				);
			$this->mod_newsfeed->add_once($data);
			$json_arr['sys_code'] = '200';
			$json_arr['sys_msg'] = '處理完成';
			$json_arr['newsfeed_id'] = $newsfeed_id;
			
		}
		echo json_encode($json_arr);
	}

	function newsfeed_list(){
		$this->load->model('mod_newsfeed');
		$member_id = $this->input->get_post('member_id');
		$tab = $this->input->get_post('tab');
		$qty = $this->input->get_post('qty');
		if($qty == ""){
			$qty = 20;
		}
		if($member_id == '' || $tab == '' ){
			$json_arr['sys_code'] = '000';
			$json_arr['sys_msg'] = '參數不足或格式錯誤';
		}elseif(!$this->mod_member->chk_once_by_id($member_id)){
			$json_arr['sys_code'] = '404';
			$json_arr['sys_msg'] = '查無此人';
		}else{
			$newsfeed_list =  $this->mod_newsfeed->get_list($member_id,$tab,$qty);
			if($newsfeed_list == false){
				$newsfeed_list = array();
			}
			$json_arr['newsfeed_list'] = $newsfeed_list;
			$json_arr['sys_code'] = '200';
			$json_arr['sys_msg'] = '處理完成';
		}
		echo json_encode($json_arr);

	}

	function newsfeed_like(){
		$this->load->model('mod_newsfeed');
		$member_id = $this->input->get_post('member_id');
		$newsfeed_id = $this->input->get_post('newsfeed_id');
		if($member_id == '' || $newsfeed_id == '' ){
			$json_arr['sys_code'] = '000';
			$json_arr['sys_msg'] = '參數不足或格式錯誤';
		}elseif(!$this->mod_member->chk_once_by_id($member_id)){
			$json_arr['sys_code'] = '404';
			$json_arr['sys_msg'] = '查無此人';
		}else{
			$this->mod_newsfeed->switch_like($member_id,$newsfeed_id);
			$json_arr['sys_code'] = '200';
			$json_arr['sys_msg'] = '處理完成';
		}
		echo json_encode($json_arr);
	}
	function newsfeed_like_list(){
		$this->load->model('mod_newsfeed');
		$newsfeed_id = $this->input->get_post('newsfeed_id');
		if( $newsfeed_id == '' ){
			$json_arr['sys_code'] = '000';
			$json_arr['sys_msg'] = '參數不足或格式錯誤';
		}else{
			$json_arr['member_list'] = $this->mod_newsfeed->newsfeed_like_list($newsfeed_id);
			$json_arr['sys_code'] = '200';
			$json_arr['sys_msg'] = '處理完成';
		}
		echo json_encode($json_arr);
	}

	function newsfeed_comment(){
		$this->load->model('mod_newsfeed');
		$newsfeed_id = $this->input->get_post('newsfeed_id');
		if($newsfeed_id == "" ){
			$json_arr['sys_code'] = '000';
			$json_arr['sys_msg'] = '參數不足或格式錯誤';
		}elseif(!$this->mod_newsfeed->chk_once($newsfeed_id)){
			$json_arr['sys_code'] = '404';
			$json_arr['sys_msg'] = '查無此動態';
		}else{
			$comment_list =  $this->mod_newsfeed->comment_list($newsfeed_id);
			if($comment_list == false){
				$comment_list = array();
			}
			$json_arr['comment_list'] = $comment_list;
			$json_arr['sys_code'] = '200';
			$json_arr['sys_msg'] = '處理完成';
		}
		echo json_encode($json_arr);

	}

	function newsfeed_comment_add(){
		$this->load->model('mod_newsfeed');
		$newsfeed_id = $this->input->get_post('newsfeed_id');
		$member_id = $this->input->get_post('member_id');
		$note = $this->input->get_post('note');
		if($newsfeed_id == "" || $member_id == "" || $note ==""){
			$json_arr['sys_code'] = '000';
			$json_arr['sys_msg'] = '參數不足或格式錯誤';
		}elseif(!$this->mod_member->chk_once_by_id($member_id)){
			$json_arr['sys_code'] = '404';
			$json_arr['sys_msg'] = '查無此人';
		}elseif(!$this->mod_newsfeed->chk_once($newsfeed_id)){
			$json_arr['sys_code'] = '404';
			$json_arr['sys_msg'] = '查無此動態';
		}else{
			$this->mod_newsfeed->comment_add($newsfeed_id,$member_id,$note);
			$json_arr['sys_code'] = '200';
			$json_arr['sys_msg'] = '處理完成';
		}
		echo json_encode($json_arr);

	}


/** -------  -------------------
*
* 好友
----------------------------****/
	function friend_mapping(){
		$this->load->model('mod_friend');
		$member_id = $this->input->get_post('member_id');
		$friend_id = $this->input->get_post('friend_id');
		
		if($member_id == '' ){
			$json_arr['sys_code'] = '000';
			$json_arr['sys_msg'] = '參數不足或格式錯誤';
		}elseif(!$this->mod_member->chk_once_by_id($member_id)){
			$json_arr['sys_code'] = '404';
			$json_arr['sys_msg'] = '查無此人';
		}else{
			$friend_list = $this->mod_friend->mapping($member_id,$friend_id);
			if($friend_list == false){
				$json_arr['friend_list'] = array();
			}else{
				$json_arr['friend_list'] = $friend_list;
			}
			// $json_arr['sql'] = $this->db->last_query();
			$json_arr['sys_code'] = '200';
			$json_arr['sys_msg'] = '處理完成';

		}
		echo json_encode($json_arr);
	}

	function friend_add(){
		$this->load->model('mod_friend');
		$member_id = $this->input->get_post('member_id');
		$friend_id = $this->input->get_post('friend_id');
		if($member_id == '' || $friend_id == ""){
			$json_arr['sys_code'] = '000';
			$json_arr['sys_msg'] = '參數不足或格式錯誤';
		}elseif(!$this->mod_member->chk_once_by_id($member_id)){
			$json_arr['sys_code'] = '404';
			$json_arr['sys_msg'] = '查無會員';
		}elseif(!$this->mod_member->chk_once_by_id($friend_id)){
			$json_arr['sys_code'] = '404';
			$json_arr['sys_msg'] = '查無好友會員狀態';
		}else{
			$this->mod_friend->add_friend($member_id,$friend_id);
			$json_arr['sys_code'] = '200';
			$json_arr['sys_msg'] = '處理完成';
			

		}
		echo json_encode($json_arr);
	}

	function friend_remove(){
		$this->load->model('mod_friend');
		$member_id = $this->input->get_post('member_id');
		$friend_id = $this->input->get_post('friend_id');
		if($member_id == '' || $friend_id == ""){
			$json_arr['sys_code'] = '000';
			$json_arr['sys_msg'] = '參數不足或格式錯誤';
		}elseif(!$this->mod_member->chk_once_by_id($member_id)){
			$json_arr['sys_code'] = '404';
			$json_arr['sys_msg'] = '查無會員';
		}elseif(!$this->mod_member->chk_once_by_id($friend_id)){
			$json_arr['sys_code'] = '404';
			$json_arr['sys_msg'] = '查無好友會員狀態';
		}else{
			$this->mod_friend->friend_remove($member_id,$friend_id);
			$json_arr['sys_code'] = '200';
			$json_arr['sys_msg'] = '處理完成';
		}
		echo json_encode($json_arr);
	}

	function friend_list(){
		$this->load->model('mod_friend');
		$member_id = $this->input->get_post('member_id');
		if($member_id == '' ){
			$json_arr['sys_code'] = '000';
			$json_arr['sys_msg'] = '參數不足或格式錯誤';
		}elseif(!$this->mod_member->chk_once_by_id($member_id)){
			$json_arr['sys_code'] = '404';
			$json_arr['sys_msg'] = '查無會員';
		}else{
			$friend_list = $this->mod_friend->get_list_info($member_id);
			if($friend_list == false){
				$json_arr['friend_list'] = array();
			}else{
				$json_arr['friend_list'] = $friend_list;
			}
			$json_arr['sys_code'] = '200';
			$json_arr['sys_msg'] = '處理完成';
		}
		echo json_encode($json_arr);
	}


	/**
	*
	* 成就
	**/
	
	function ach_list(){
		$this->load->model('mod_ach');
		$member_id = $this->input->get_post('member_id');
		if($member_id == '' ){
			$json_arr['sys_code'] = '000';
			$json_arr['sys_msg'] = '參數不足或格式錯誤';
		}elseif(!$this->mod_member->chk_once_by_id($member_id)){
			$json_arr['sys_code'] = '404';
			$json_arr['sys_msg'] = '查無會員';
		}else{
			
			$json_arr['ach_list']=$this->mod_ach->get_list($member_id);
			$member = $this->mod_member->get_once(array("member_id"=>$member_id));
			$json_arr['score'] = $member['point'];
			$json_arr['totalDistance'] = $this->mod_ach->totalDistance($member_id);
			$json_arr['totalRun'] = $this->mod_ach->totalRun($member_id);
			$json_arr['totalTime'] = $this->mod_ach->totalTime($member_id);
			// $json_arr['sql'] = $this->db->last_query();
			$json_arr['sys_code'] = '200';
			$json_arr['sys_msg'] = '處理完成';
		}
		echo json_encode($json_arr);
	}
	function ach_log_last_5(){
		$this->load->model('mod_ach');
		$member_id = $this->input->get_post('member_id');
		if($member_id == '' ){
			$json_arr['sys_code'] = '000';
			$json_arr['sys_msg'] = '參數不足或格式錯誤';
		}elseif(!$this->mod_member->chk_once_by_id($member_id)){
			$json_arr['sys_code'] = '404';
			$json_arr['sys_msg'] = '查無會員';
		}else{
			
			$json_arr['list']=$this->mod_ach->get_log_list($member_id);
			// $json_arr['sql'] = $this->db->last_query();
			$json_arr['sys_code'] = '200';
			$json_arr['sys_msg'] = '處理完成';
		}
		echo json_encode($json_arr);
	}



}
?>