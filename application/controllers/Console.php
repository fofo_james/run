<?php
class Console extends CI_Controller{
		
		function __construct() {
			parent::__construct();
			
		}
		/**
		* 首頁登入登出
		*
		*
		**/
		function index(){
			if($this->mod_manager->chk_login_status() == true){
				
				// $view_data = array(
				// 	"title"=>"控制台首頁",
				// 	"path"=>'console/index'
				// 	);
				// $this->load->view('console/layout',$view_data);
				redirect(base_url('console/member_list'));
			}else{
				redirect(base_url('console/login'));
			}
		}

		function login(){
			if($this->mod_manager->chk_login_status() == true){
				redirect(base_url('console/index'));
				
			}else{
				
				$this->load->view('console/login');
				
			}
		}
		function logout(){
			$this->mod_manager->do_logout();
			$this->load->view('console/login');
		}
		
		/**
		*	會員
		*
		***********/
		function member_list(){
			if($this->mod_manager->chk_login_status() == true){
				$this->load->model('mod_member');
				$view_data = array(
					"title"=>"會員清單",
					"path"=>'console/member_list',
					"datalist"=>$this->mod_member->get_list()
					);
				$this->load->view('console/layout',$view_data);
				// redirect(base_url('console/member_list'));
			}else{
				redirect(base_url('console/login'));
			}
		}

		function member_add(){
			if($this->mod_manager->chk_login_status() == true){
				$this->load->model('mod_member');
				$do = $this->input->post('do');
				if($do == ""){
					$view_data = array(
						"title"=>"新增會員",
						"path"=>'console/member_add',
						"datalist"=>$this->mod_member->get_list()
						);
					$this->load->view('console/layout',$view_data);
				}else{
					$email = $this->input->get_post('email');
					$member_id = $this->input->get_post('member_id');
					$password = $this->input->get_post('password');
					$username = $this->input->get_post('username');
					$birthday = $this->input->get_post('birthday');
					$gender = $this->input->get_post('gender');
					$height = $this->input->get_post('height');
					$weight = $this->input->get_post('weight');
					
					$avator_file = "avator/".$member_id.'.png';
					copy($_FILES['avator']['tmp_name'],$avator_file);

					$background_file = "background/".$member_id.'.png';
					copy($_FILES['background']['tmp_name'],$background_file);
					
					$data = array(
						"member_id"=>$member_id,
						"email"=>$email,
						"password"=>$password,
						"username"=>$username,
						"birthday"=>$birthday,
						"gender"=>$gender,
						"height"=>$height,
						"weight"=>$weight,
						"avator"=>base_url($avator_file),
						"background"=>base_url($background_file)
						);
					$this->mod_member->add_once($data);
					redirect(base_url('console/member_list'));
				}
				
				// redirect(base_url('console/member_list'));
			}else{
				redirect(base_url('console/login'));
			}
		}


		/** **
		* newsfeed
		**/
		function newsfeed_list(){
			if($this->mod_manager->chk_login_status() == true){
				$this->load->model('mod_newsfeed');
				$member_id = $this->input->get_post('member_id');
				$view_data = array(
					"title"=>"動態清單",
					"path"=>'console/newsfeed_list',
					"datalist"=>$this->mod_newsfeed->get_list($member_id,'my',100)
					);
				$this->load->view('console/layout',$view_data);
				// redirect(base_url('console/member_list'));
			}else{
				redirect(base_url('console/login'));
			}
		}

		function newsfeed_add(){
			if($this->mod_manager->chk_login_status() == true){
				$this->load->model('mod_newsfeed');
				$do = $this->input->post('do');
				if($do == ""){
					$view_data = array(
						"title"=>"新增動態",
						"path"=>'console/newsfeed_add',
						);
					$this->load->view('console/layout',$view_data);
				}else{
					
					$newsfeed_id = uniqid();
					$member_id = $this->input->get_post('member_id');
					$times = $this->input->get_post('times');
					$milles = $this->input->get_post('milles');
					$note = $this->input->get_post('note');
					$file = "newsfeed/".$newsfeed_id.'.png';
					copy($_FILES['picture']['tmp_name'],$file);
					if(file_exists($file)){
						$picture = base_url($file);
					}else{
						$picture = '';
					}

					$data = array(
						"newsfeed_id"=>$newsfeed_id,
						"member_id"=>$member_id,
						"times"=>$times,
						"milles"=>$milles,
						"note"=>$note,
						"picture"=>$picture,
						"date_time"=>date("Y-m-d H:i:s")
						);
					$this->mod_newsfeed->add_once($data);
					redirect(base_url('console/newsfeed_list?member_id='.$member_id));
				}
				
				// redirect(base_url('console/member_list'));
			}else{
				redirect(base_url('console/login'));
			}
		}

		


}
?>