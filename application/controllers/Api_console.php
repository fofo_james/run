<?php 
class Api_console extends CI_Controller{
	function login(){
		$id = $this->input->post('id');
		$pwd = $this->input->post('pwd');
		if($id == "" || $pwd == ""){
			$json_arr['sys_code'] = '000';
			$json_arr['sys_msg'] = '資料不足';
		}else{
			if($this->mod_manager->chk_used($id)){
				if($this->mod_manager->chk_login($id,$pwd)){
					$this->mod_manager->do_login($id);
					$json_arr['sys_code'] = '200';
					$json_arr['sys_msg']=  '登入成功';
				}else{
					$json_arr['sys_code'] = '500';
					$json_arr['sys_msg'] = '登入失敗，請檢查帳號密碼是否正確';
				}
			}else{
				$json_arr['sys_code'] = '404';
				$json_arr['sys_msg'] = '查無此人';
			}
		}
		echo json_encode($json_arr);
	}


	function remove_member(){
		$this->load->model('mod_member');
		$member_id = $this->input->get_post('member_id');
		$this->mod_member->remove_once($member_id);
		$json_arr['sys_code'] = '200';
		$json_arr['sys_msg']=  '成功';
		echo json_encode($json_arr);
	}

	function remove_newsfeed(){
		$this->load->model('mod_newsfeed');
		$newsfeed_id = $this->input->get('newsfeed_id');
		$this->mod_newsfeed->remove_once($newsfeed_id);
		$json_arr['sys_code'] = '200';
		$json_arr['sys_msg']=  '成功';
		echo json_encode($json_arr);
	}
	





}
?>