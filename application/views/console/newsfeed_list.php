<div class="page-content">
	<div class="page-header">
		<h1>
			控制台
			<small>
			<i class="icon-double-angle-right"></i><?=$title;?></small>
		</h1>
	</div><!-- /.page-header -->
	<div class="row">
		<button class="btn" id="add">
			<i class="icon-pencil align-top bigger-125"></i>新增
		</button>
	</div>
	<div class="hr"></div>
	<div class="row">
		<div class="col-xs-12">
			<div class="col-xs-12">
			<table class="table table-striped table-bordered table-hover">
				<tr>
					<th>times</th>
					<th>milles</th>
					<th>note</th>
					<th>picture</th>
					<th>date_time</th>
					<th></th>
				</tr>
				<?php foreach ($datalist as $key => $value) :?>
					<tr>
						<td><?=$value['times']?></td>
						<td><?=$value['milles']?></td>
						<td><?=$value['note']?></td>
						<td><img src="<?=$value['picture']?>" width="50"></td>
						<td><?=$value['date_time']?></td>
						<td>
							<button class="btn btn-link icon-remove" newsfeed_id="<?=$value['newsfeed_id']?>"> remove </button>
						</td>
					</tr>
				<?php endforeach;?>
			</table>
			
		</div>
		</div>
	</div>



	
</div>


<script>
	$(function(){
		$("body").on("click",".icon-remove",function(){
			if(confirm("您確定要刪除資料？刪除後無法復原")){
				$.getJSON('./api_console/remove_newsfeed',{
					"newsfeed_id":$(this).attr('newsfeed_id')
				},function(json){
					alert(json.sys_msg);
					location.reload();
				})
			}
		});
		$("body").on("click","#add",function(){
			location.href="./console/newsfeed_add?member_id=<?=$_GET['member_id']?>";
		})
	})

</script>