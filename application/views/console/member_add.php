<div class="page-content">
	<div class="page-header">
		<h1>
			控制台
			<small>
			<i class="icon-double-angle-right"></i><?=$title;?></small>
		</h1>
	</div><!-- /.page-header -->
	<div class="row">
		<form class="form-horizontal"  role="form" method="post" enctype="multipart/form-data">
			<input type="hidden" value="do" name="do">
			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> ＊會員編號 </label>
				<div class="col-sm-9">
					<input name="member_id" value="<?= "M".time();?>" type="text" id="member_id" placeholder="" class="col-xs-10 col-sm-5" readonly="readonly">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> ＊email </label>
				<div class="col-sm-9">
					<input name="email" value="" type="text" id="email" placeholder="" class="col-xs-10 col-sm-5" >
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> ＊password </label>
				<div class="col-sm-9">
					<input name="password" value="" type="text" id="password" placeholder="" class="col-xs-10 col-sm-5">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> birthday </label>
				<div class="col-sm-9">
					<input name="birthday" value="" type="text" id="birthday" placeholder="" class="col-xs-10 col-sm-5" >
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> ＊username </label>
				<div class="col-sm-9">
					<input name="username" value="" type="text" id="username" placeholder="" class="col-xs-10 col-sm-5" >
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> height </label>
				<div class="col-sm-9">
					<input name="height" value="" type="text" id="height" placeholder="" class="col-xs-10 col-sm-5" >
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> weight </label>
				<div class="col-sm-9">
					<input name="weight" value="" type="text" id="weight" placeholder="" class="col-xs-10 col-sm-5" >
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> gender </label>
				<div class="col-sm-9">
					<select name="gender" id="gender">
						<option value="Male">Male</option>
						<option value="Female">Female</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> ＊avator </label>
				<div class="col-sm-9">

					<input id="avator" type="file" name="avator"  />
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> ＊background </label>
				<div class="col-sm-9">

					<input id="background" type="file" name="background"  />
				</div>
			</div>
			
			<div class="clearfix form-actions">
				<div class="col-md-offset-3 col-md-9">
					<button id="submit" class="btn btn-info" type="submit">
						<i class="icon-ok bigger-110"></i>Submit
					</button>

											&nbsp; &nbsp; &nbsp;
					<button class="btn" type="reset">
						<i class="icon-undo bigger-110"></i>Reset
					</button>
				</div>
			</div>
		</form>
	</div>
</div>







<script type="text/javascript">
   
</script>
