<div class="page-content">
	<div class="page-header">
		<h1>
			控制台
			<small>
			<i class="icon-double-angle-right"></i><?=$title;?></small>
		</h1>
	</div><!-- /.page-header -->
	<div class="row">
		<button class="btn" id="add">
			<i class="icon-pencil align-top bigger-125"></i>新增
		</button>
	</div>
	<div class="hr"></div>
	<div class="row">
		<div class="col-xs-12">
			<div class="col-xs-12">
			<table class="table table-striped table-bordered table-hover">
				<tr>
					<th>member_id</th>
					<th>email</th>
					<th>username</th>
					<th>avator</th>
					<th>fbid</th>
					<th></th>
				</tr>
				<?php foreach ($datalist as $key => $value) :?>
					<tr>
						<td><?=$value['member_id']?></td>
						<td><?=$value['email']?></td>
						<td><?=$value['username']?></td>
						<td><img src="<?=$value['avator']?>" width="50"></td>
						<td><?=$value['fbid']?></td>
						<td>
							<button class="btn btn-link icon-remove" member_id="<?=$value['member_id']?>"> remove </button>
							<button class="btn btn-link icon-list" member_id="<?=$value['member_id']?>"> newsfeed </button>
						</td>
					</tr>
				<?php endforeach;?>
			</table>
			
		</div>
		</div>
	</div>



	
</div>


<script>
	$(function(){
		$("body").on("click",".icon-remove",function(){
			if(confirm("您確定要刪除資料？刪除後無法復原")){
				$.getJSON('./api_console/remove_member',{
					"member_id":$(this).attr('member_id')
				},function(json){
					alert(json.sys_msg);
					location.reload();
				})
			}
		});
		$("body").on("click","#add",function(){
			location.href="./console/member_add";
		})
		$("body").on("click",".icon-list",function(){
			location.href="./console/newsfeed_list?member_id="+$(this).attr('member_id');
		})
	})

</script>