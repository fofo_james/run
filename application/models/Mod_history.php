<?php 
class Mod_history extends CI_Model{
	function add_once($data){
		$this->db->insert("history",$data);
		return $this->db->insert_id();
	}
	function get_list($member_id,$sensor_mac,$qty){
		$this->db->where("member_id",$member_id);
		if($sensor_mac != ""){
			$this->db->where("sensor_mac",$sensor_mac);
		}
		$this->db->limit($qty);
		$this->db->order_by('date_time',"desc");
		$res = array();
		foreach ($this->db->get('history')->result_array() as $key => $value) {
			# code...
			
			$res[] = array(
				"hid"=>$value['hid'],
				"milles"=>$value['milles'],
				"date_time"=>strtotime($value['date_time']),
				"foreProfile"=>base_url('./csv/'.$value['hid'].'_f.csv'),
				"heelProfile"=>base_url('./csv/'.$value['hid'].'_h.csv'),
				);
		}
		return $res;
	}
	function get_once($hid){
		$this->db->where('hid',$hid);
		return $this->db->get('history')->row_array();
	}
	

}
?>