<?php
class Mod_member extends CI_Model{
	//-----MOD-----------------------------------------
	// FB註冊會員
	function fb_reg($fbid,$email){
		$member_id = "M".time();
		$this->db->insert('member_main',array('fbid'=>$fbid,"email"=>$email,'member_id'=>$member_id));
		return $member_id;
	}
	function chk_login_general($email,$password){
		$this->db->where('email',$email);
		$this->db->where('password',$password);
		if($this->db->count_all_results('member_main') == 0){
			return false;
		}else{
			$m = $this->get_once(array('email'=>$email));
			return $m['member_id'];
		}
	}
	function chk_login_fb($email,$fbid){
		$this->db->where('email',$email);
		$this->db->where('fbid',$fbid);
		if($this->db->count_all_results('member_main') == 0){
			return false;
		}else{
			$m = $this->get_once(array('email'=>$email));
			return $m['member_id'];
		}
	}
	function update_score($member_id,$score,$class){
		$this->db->where('member_id',$member_id);
		$this->db->update('member_main',array('point'=>$score,"class"=>$class));
		return true;
	}

	//-----MDB------------------------------------------
	function chk_once($email){
		$this->db->where('email',$email);
		if($this->db->count_all_results('member_main') == 0){
			return false;
		}else{
			return $this->get_once(array('email'=>$email));
		}
	}
	function chk_once_by_fbid($fbid){
		$this->db->where('fbid',$fbid);
		if($this->db->count_all_results('member_main') == 0){
			return false;
		}else{
			return $this->get_once(array('fbid'=>$fbid));
		}
	}
	function chk_once_by_id($member_id){
		$this->db->where('member_id',$member_id);
		if($this->db->count_all_results('member_main') == 0){
			return false;
		}else{
			return $this->get_once(array('member_id'=>$member_id));
		}
	}
	function add_once($data){
		$this->db->insert('member_main',$data);
		return $this->db->insert_id();
	}
	function update_once($member_id,$data){
		$this->db->where('member_id',$member_id);
		$this->db->update('member_main',$data);
		return true;
	}
	function get_once($where){
		$this->db->where($where);
		$m = $this->db->get('member_main')->row_array();
		$res = array(
			"member_id"=>$m['member_id'],
			"email"=>$m['email'],
			"username"=>$m['username'],
			"birthday"=>$m['birthday'],
			"gender"=>$m['gender'],
			"height"=>$m['height'],
			"weight"=>$m['weight'],
			"avator"=>$m['avator'],
			"point"=>$m['point'],
			"background"=>$m['background'],
			"class"=>$m['class']
			);
		return $res;
	}
	function get_list(){
		$this->db->order_by('member_id','desc');
		return $this->db->get('member_main')->result_array();
	}
	function remove_once($member_id){
		$this->db->where('member_id',$member_id);
		$this->db->delete("member_main");
		return true;
	}
}
?>