<?php 
class Mod_manager extends CI_Model {
	//---- model ---------
	//
	function chk_login_status(){
		// print_r($this->session->userdata('__ci_last_regenerate'));
		if($this->session->userdata('manager_name') !== null){
			return true;
		}else{
			return false;
		}
	}
	function chk_login($id,$pwd){
		$this->db->where('manager_id',$id);
		$this->db->where('manager_pwd',$pwd);
		if($this->db->count_all_results("manager_main") == 0){
			return false;
		}else{
			return true;
		}
	}
	function do_login($id){
		// echo 'do_login';
		$manager =  $this->get_once($id);
		// print_r($manager);
		// 設定session
		$session_arr = array(
				"manager_name"=>$manager['manager_name'],
				"manager_id"=>$manager['manager_id'],
				"manager_level"=>$manager['level'],
				"console_login_status"=>true,
				);
		
		
		$this->session->set_userdata( $session_arr );
		// 設定登入時間
		$this->db->where('manager_id',$id);
		$this->db->update("manager_main",array('last_login'=>date("Y-m-d H:i:s")));
		return true;
	}
	function do_logout(){
		$this->session->unset_userdata('manager_name');
		$this->session->unset_userdata('mamager_id');
		$this->session->unset_userdata('console_login_status');
		return true;
	}

	function get_ticket_list(){
		$this->db->where('level',1);
		$this->db->or_where('level',2);
		return $this->db->get('manager_main')->result_array();
	}


	function add_once($data){
		$this->db->insert('manager_main',$data);
		return true;
	}








	//--- db -------------------------------------------------------------
	function chk_used($id){
		$this->db->where("manager_id",$id);
		if($this->db->count_all_results("manager_main") == 0){
			return false;
		}else{
			return true;
		}
	}
	function get_once($id){
		$this->db->where("manager_id",$id);
		return $this->db->get("manager_main")->row_array();
	}
	function update_once($id,$data){
		// echo $id;
		$this->db->where("manager_id",$id);
		$this->db->update("manager_main",$data);
		return true;
	}
	function remove_once($id){
		// echo $id;
		$this->db->where("manager_id",$id);
		$this->db->delete("manager_main");
		return true;
	}

}
?>