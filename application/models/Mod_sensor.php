<?php
class Mod_sensor extends CI_Model{
	function chk_sensor($sensor_mac){
		$this->db->where('sensor_mac',$sensor_mac);
		if($this->db->count_all_results('member_sensor') == 0){
			return false;
		}else{
			return true;
		}
	}

	function mapping_sensor($sensor_mac,$member_id,$device_name){
		$data = array(
			"sensor_mac"=>$sensor_mac,
			"member_id"=>$member_id,
			"device_name"=>$device_name,
			);
		$this->db->insert("member_sensor",$data);
	}

	function get_list($member_id){
		$this->db->where('member_id',$member_id);
		return $this->db->get('member_sensor')->result_array();
	}

	function unlink_sensor($sensor_mac,$member_id){
		$this->db->where('member_id',$member_id);
		$this->db->where('sensor_mac',$sensor_mac);
		$this->db->delete('member_sensor');
		return true;
	}
}
?>