<?php
class Mod_friend extends CI_Model{
	function mapping($member_id,$fbid){
		// $this->db->where('member_id',$member_id);
		$f = $this->get_list($member_id);
		$f_arr = explode(',',$fbid);
		$this->db->where_in('fbid', $f_arr);
		
		$this->db->where_not_in('member_id',$f);
		return $this->db->get('member_main')->result_array();
	}
	function get_list($member_id){
		$this->db->where("a",$member_id);
		// $f = $this->db->get('friend_map')->result_array();
		$res = array();
		foreach ($this->db->get('friend_map')->result_array() as $key => $value) {
			# code...
			$res[] = $value["b"];
		}
		// echo $this->db->last_query();
		// print_r($res);
		return $res;
	}
	function get_list_info($member_id){
		$fl = $this->get_list($member_id);
		$res = array();
		if(count($fl) > 0){
			$this->db->where_in('member_id',$fl);
			foreach ($this->db->get('member_main')->result_array() as $key => $value) {
				# code...
				$res[] = array(
					"member_id"=>$value['member_id'],
					"name"=>$value['username'],
					"avator"=>$value['avator'],
					"point"=>$value['point'],
					);
			}
		}
		
		// print_r($res);
		return $res;
	}
	function chk_friend($member_id,$friend_id){
		$this->db->where(array("a"=>$member_id,"b"=>$friend_id));
		if($this->db->count_all_results('friend_map') == 0){
			return false;
		}else{
			return true;
		}
	}
	function add_friend($member_id,$friend_id){
		$this->db->insert('friend_map',array("a"=>$member_id,"b"=>$friend_id));
		$this->db->insert('friend_map',array("b"=>$member_id,"a"=>$friend_id));
		return true;
	}
	function friend_remove($member_id,$friend_id){
		$this->db->where(array("a"=>$member_id,"b"=>$friend_id))->delete("friend_map");
		$this->db->where(array("b"=>$member_id,"a"=>$friend_id))->delete("friend_map");
		return true;
	}
}
?>