<?php 
class Mod_ach extends CI_Model{
	function get_list($member_id){
		$this->db->where("member_id",$member_id);
		if($this->db->count_all_results("ach") == 0){
			$this->db->insert("ach",array("member_id"=>$member_id));
		}

		$totalDistance = $this->totalDistance($member_id);
		$totalRun = $this->totalRun($member_id);
		$totalTime = (($this->totalTime($member_id))/60)/60;
		$singleRunDistance = $this->singleRunDistance($member_id);
		$newsfeedPublishTime = $this->newsfeedPublishTime($member_id);
		$uploadPhotoTime = $this->uploadPhotoTime($member_id);
		$friendNumber = $this->friendNumber($member_id);

		$this->db->where("member_id",$member_id);
		$res = array();

		foreach ($this->db->get("ach")->row_array() as $key => $value) {
			

			if($value == "0"){
				switch ($key) {
					// 總里程 ===================
					case 'totalDistance_50':
						# code...
						if($totalDistance >= 50){
							$value = "1";
							$this->set_ach($key,$member_id);
							$this->set_score($member_id,$key,100);
						}
						break;
					case 'totalDistance_200':
						# code...
						if($totalDistance >= 200){
							$value = "1";
							$this->set_ach($key,$member_id);
							$this->set_score($member_id,$key,500);
						}
						break;
					case 'totalDistance_1000':
						# code...
						if($totalDistance >= 1000){
							$value = "1";
							$this->set_ach($key,$member_id);
							$this->set_score($member_id,$key,1000);
						}
						break;
					//跑步次數 ===================
					case 'totalRun_10':
						# code...
						if($totalRun >= 10){
							$value = "1";
							$this->set_ach($key,$member_id);
							$this->set_score($member_id,$key,100);
						}
						break;
					case 'totalRun_50':
						# code...
						if($totalRun >= 50){
							$value = "1";
							$this->set_ach($key,$member_id);
							$this->set_score($member_id,$key,500);
						}
						break;
					case 'totalRun_100':
						# code...
						if($totalRun >= 100){
							$value = "1";
							$this->set_ach($key,$member_id);
							$this->set_score($member_id,$key,1000);
						}
						break;

					// 總跑步時間 ===================
					case 'totalTime_10':
						# code...
						if($totalTime >= 10){
							$value = "1";
							$this->set_ach($key,$member_id);
							$this->set_score($member_id,$key,100);
						}
					break;
					case 'totalTime_50':
						# code...
						if($totalTime >= 50){
							$value = "1";
							$this->set_ach($key,$member_id);
							$this->set_score($member_id,$key,500);
						}
					break;
					case 'totalTime_100':
						# code...
						if($totalTime >= 100){
							$value = "1";
							$this->set_ach($key,$member_id);
							$this->set_score($member_id,$key,1000);
						}
					break;

					// 單次跑步里 ===================
					case 'singleRunDistance_5':
						# code...
						if($singleRunDistance >= 5){
							$value = "1";
							$this->set_ach($key,$member_id);
							$this->set_score($member_id,$key,100);
						}
					break;
					case 'singleRunDistance_15':
						# code...
						if($singleRunDistance >= 15){
							$value = "1";
							$this->set_ach($key,$member_id);
							$this->set_score($member_id,$key,500);
						}
					break;
					case 'singleRunDistance_25':
						# code...
						if($singleRunDistance >= 25){
							$value = "1";
							$this->set_ach($key,$member_id);
							$this->set_score($member_id,$key,1000);
						}
					break;


					// 發佈Newsfeed數量達 newsfeedPublishTime ===================
					case 'newsfeedPublishTime_10':
						# code...
						if($newsfeedPublishTime >= 10){
							$value = "1";
							$this->set_ach($key,$member_id);
							$this->set_score($member_id,$key,100);
						}
					break;
					case 'newsfeedPublishTime_50':
						# code...
						if($newsfeedPublishTime >= 50){
							$value = "1";
							$this->set_ach($key,$member_id);
							$this->set_score($member_id,$key,500);
						}
					break;
					case 'newsfeedPublishTime_100':
						# code...
						if($newsfeedPublishTime >= 100){
							$value = "1";
							$this->set_ach($key,$member_id);
							$this->set_score($member_id,$key,1000);
						}
					break;

					// 上傳照片數量達  ===================
					case 'uploadPhotoTime_5':
						# code...
						if($uploadPhotoTime >= 5){
							$value = "1";
							$this->set_ach($key,$member_id);
							$this->set_score($member_id,$key,100);
						}
					break;
					case 'uploadPhotoTime_25':
						# code...
						if($uploadPhotoTime >= 25){
							$value = "1";
							$this->set_ach($key,$member_id);
							$this->set_score($member_id,$key,500);
						}
					break;
					case 'uploadPhotoTime_50':
						# code...
						if($uploadPhotoTime >= 50){
							$value = "1";
							$this->set_ach($key,$member_id);
							$this->set_score($member_id,$key,1000);
						}
					break;

					// 好友數量達  ===================
					case 'friendNumber_5':
						# code...
						if($friendNumber >= 5){
							$value = "1";
							$this->set_ach($key,$member_id);
							$this->set_score($member_id,$key,100);
						}
					break;
					case 'friendNumber_25':
						# code...
						if($friendNumber >= 25){
							$value = "1";
							$this->set_ach($key,$member_id);
							$this->set_score($member_id,$key,500);
						}
					break;
					case 'friendNumber_50':
						# code...
						if($friendNumber >= 50){
							$value = "1";
							$this->set_ach($key,$member_id);
							$this->set_score($member_id,$key,1000);
						}
					break;
					
					default:
						# code...
						break;
				}
			}


			if($key != "member_id"){
				$res[$key] = $value;
			}
			
		}
		return $res;
	}

	function set_ach($key,$member_id){
		$this->db->where("member_id",$member_id);
		$this->db->update("ach",array($key=>1));

		$data = array(
			"member_id"=>$member_id,
			"ach_id"=>$key,
			"date_time"=>date("Y-m-d H:i:s")
			);
		$this->db->insert("ach_log",$data);
		return true;
	}

	function set_score($member_id,$info,$point){
		$data = array(
			"member_id"=>$member_id,
			"info"=>$info,
			"point"=>$point,
			"date_time"=>date("Y-m-d H:i:s")
			);
		$this->db->insert("score_log",$data);
		$sum_arr = $this->db->where('member_id',$member_id)->select_sum('point')->get('score_log')->row_array();
		$sum = $sum_arr['point'];
		$this->db->where('member_id',$member_id);
		$this->db->update('member_main',array('point'=>$sum));
		// print_r($sum_arr);
	}
	function get_log_list($member_id){
		$this->db->limit(5);
		$this->db->order_by("date_time","desc");
		$this->db->where('member_id',$member_id);
		return $this->db->get("ach_log")->result_array();

	}


	// 取得總里程
	function totalDistance($member_id){
		$this->db->where('member_id',$member_id);
		$m = 0;
		foreach ($this->db->get('history')->result_array() as $key => $value) {
			// print_r($value);
			# code...
			$m = $m + $value['milles'];
		}
		return $m;
	}
	// 總跑步次數
	function totalRun($member_id){
		$this->db->where('member_id',$member_id);
		return $this->db->count_all_results('history');
	}

	function totalTime($member_id){
		$this->db->where('member_id',$member_id);
		$t = 0;
		foreach ($this->db->get('history')->result_array() as $key => $value) {
			// print_r($value);
			# code...
			$t = $t + $value['times'];
		}
		
		return $t;
	}
	// 單次跑步里程達
	function singleRunDistance($member_id){
		$this->db->where('member_id',$member_id);
		$this->db->order_by('milles','desc');
		$this->db->limit(1);
		$h = $this->db->get('history')->row_array();
		return $h["milles"];
	}
	// 發佈Newsfeed數量達
	function newsfeedPublishTime($member_id){
		$this->db->where('member_id',$member_id);
		return $this->db->count_all_results('newsfeed');
	}

	function uploadPhotoTime($member_id){
		$this->db->where('member_id',$member_id);
		$this->db->where('picture !=',"");
		return $this->db->count_all_results('newsfeed');
	}
	function friendNumber($member_id){
		$this->db->where('a',$member_id);
		return $this->db->count_all_results("friend_map");
	}

}
?>