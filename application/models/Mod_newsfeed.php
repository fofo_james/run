<?php
class Mod_newsfeed extends CI_Model{
	function add_once($data){
		$this->db->insert('newsfeed',$data);
		return $this->db->insert_id();
	}
	function remove_once($newsfeed_id){
		$this->db->where("newsfeed_id",$newsfeed_id);
		$this->db->delete("newsfeed");
		return true;
	}
	function chk_once($newsfeed_id){
		$this->db->where("newsfeed_id",$newsfeed_id);
		if($this->db->count_all_results("newsfeed") == 0){
			return false;
		}else{
			return true;
		}
	}
	function get_list($member_id,$tab,$qty){
		// echo $tab;
		$res = array();
		switch ($tab) {
			case 'my':
				# code...
			// echo "my";
			$this->db->where('member_id',$member_id);
			break;
			case 'friends':
				# code...
			$this->load->model('mod_friend');
			$friends = $this->mod_friend->get_list($member_id);
			if(count($friends) > 0){
				$this->db->where_in('member_id',$friends);
			}else{
				return $res;
			}
			
			$this->db->where("member_id !=",$member_id);
			break;
			case 'all':
			$this->db->where("member_id !=",$member_id);
			
			break;
			
			
		}
		$this->db->order_by('date_time','desc');
		$this->db->limit($qty);
		
		$this->load->model('mod_member');
		foreach ($this->db->get('newsfeed')->result_array() as $key => $value) {
			# code...
			// echo $this->db->last_query();
			// print_r($value);
			$mem = $this->mod_member->get_once(array("member_id"=>$value["member_id"]));
			$res[] = array(
				"newsfeed_id"=>$value['newsfeed_id'],
				"member_id"=>$value['member_id'],
				"member_name"=>$mem['username'],
				"member_avator"=>$mem['avator'],
				"times"=>$value['times'],
				"milles"=>$value['milles'],
				"note"=>$value['note'],
				"picture"=>$value['picture'],
				"is_like"=>$this->chk_like($member_id,$value['newsfeed_id']),
				"date_time"=>$value['date_time'],
				);
		}
		return $res;
	}

	function comment_list($newsfeed_id){
		$this->db->where("newsfeed_id",$newsfeed_id);
		$this->db->order_by('date_time','desc');
		$res = array();
		$this->load->model('mod_member');
		foreach ($this->db->get("comment")->result_array() as $key => $value) {
			# code...
			$mem = $this->mod_member->get_once(array("member_id"=>$value['member_id']));
			$res[] = array(
				"member_id"=>$value['member_id'],
				"member_name"=>$mem['username'],
				"member_avator"=>$mem['avator'],
				"date_time"=>$value['date_time'],
				"note"=>$value['note']
				);
		}
		return $res;
	}

	function comment_add($newsfeed_id,$member_id,$note){
		$data = array(
			"sn"=>uniqid(),
			"newsfeed_id"=>$newsfeed_id,
			"member_id"=>$member_id,
			"date_time"=>date("Y-m-d H:i:s"),
			"note"=>$note,
			);
		$this->db->insert("comment",$data);
		return $this->db->insert_id();
	}


	function chk_like($member_id,$newsfeed_id){
		$this->db->where('member_id',$member_id);
		$this->db->where('newsfeed_id',$newsfeed_id);
		if($this->db->count_all_results('newsfeed_like') == 0){
			return false;
		}else{
			return true;
		}
	}

	function switch_like($member_id,$newsfeed_id){
		$this->db->where('member_id',$member_id);
		$this->db->where('newsfeed_id',$newsfeed_id);
		if($this->db->count_all_results('newsfeed_like') == 0){
			
			$data = array(
				"member_id"=>$member_id,
				"newsfeed_id"=>$newsfeed_id,
				"date_time"=>date("Y-m-d H:i:s"),
				);
			$this->db->insert("newsfeed_like",$data);
		}else{
			$this->db->where('member_id',$member_id);
			$this->db->where('newsfeed_id',$newsfeed_id);
			$this->db->delete("newsfeed_like");
		}

	}

	function newsfeed_like_list($newsfeed_id){
		$this->db->where('newsfeed_id',$newsfeed_id);
		$this->db->order_by('date_time','desc');
		$res  = array();
		foreach ($this->db->get("newsfeed_like")->result_array() as $key => $value) {
			# code...
			$m = $this->mod_member->get_once(array('member_id'=>$value['member_id']));
			$res[] = array(
				"member_id"=>$value['member_id'],
				"username"=>$m['username'],
				"avator"=>$m['avator'],
				"date_time"=>$value['date_time']
				);
		}
		if($res == false){
			return array();
		}else{
			return $res;
		}
	}

	
}
?>